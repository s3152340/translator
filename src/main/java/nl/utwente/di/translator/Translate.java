package nl.utwente.di.translator;

public class Translate {
    double translate(double celsius){
        return (celsius*1.8) + 32;
    }
}
