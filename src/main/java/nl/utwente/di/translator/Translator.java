package nl.utwente.di.translator;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;


public class Translator extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Translate translate;
	
    public void init() throws ServletException {
    	translate = new Translate();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius to Fahrenheit";

    String celsiusParam = request.getParameter("celsius");
    double actualCelsius = 0;
    try{
        actualCelsius = Double.parseDouble(celsiusParam);
    }catch (NumberFormatException e){
        out.println("<p>Error: Invalid input.</p>");
        return;
    }
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   actualCelsius + "\n" +
                "  <P>Fahrenheit: " +
                   translate.translate(actualCelsius) +
                "</BODY></HTML>");
  }
}
